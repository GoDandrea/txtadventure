#ifndef HASH_H
#define HASH_H

#include <stdlib.h>
#include "elemento.h"

TabSim criaTab (int tam);
void destroiTab (TabSim t);
int insereTab (TabSim t, char *n, Elemento *val);
Elemento* buscaTab (TabSim t, char *n);
int retiraTab (TabSim t, char *n);

Lista criaLista ();
void destroiLista (Lista l);
Elo* insereLista (Lista l, void *val);
Elemento* buscaLista (Lista l, char *n);
Elemento* retiraLista (Lista l, Elemento *val);

#endif