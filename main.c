#include "gameData.h"

void examinar (void) {

  char id[64];
  id[0] = getc (stdin);
  Elemento *alvo;
  gets (id);

  if (strcmp(id, "aqui") == 0) {
    puts ("");
    puts (PROTAG.posicao->longa);
    puts ("> Saídas:");
    listaSaidas (*PROTAG.posicao);
    puts ("> Objetos à vista:");
    listaConteudo (*PROTAG.posicao);
  }

  else {
    alvo = buscaLista (PROTAG.posicao->conteudo, id);
    if (alvo != NULL)
      puts (alvo->longa);
    else {
      puts ("> Alvo de inspeção não encontrado.\n> Alvos encontrados:");
      listaConteudo (*PROTAG.posicao);
    }
  }
}

void mover (void) {

  char id[64];
  id[0] = getc (stdin);
  Elemento *alvo;
  gets(id);  

  alvo = buscaLista (PROTAG.posicao->detalhe, id);
  puts ("");
  if (alvo != NULL)
    PROTAG.posicao = alvo;
  else {
    puts ("> Destino não encontrado.\n> Destinos encontrados:");
    listaSaidas (*PROTAG.posicao);
  }
}

/* verifica se um objeto está presente e visível */
/* retorna 1 se no local, 2 se no inventário, 0 se não existir*/
int presente(char *nome) {
  if (getsym(inventario, nome)) return 2;
  if (getsym(Posic->cont, nome)) return 1;
  return 0;
}


int main() {

  char c;
  TabSim universo = criaTab (Total_Elementos);

  intro();
  iniciaProtag();
  iniciaLugares(universo);
  iniciaObjetos(universo);

  while (1) {

    if (!PROTAG.posicao->conhecido) {
      PROTAG.posicao->conhecido = 1;
      puts (PROTAG.posicao->longa);
      puts ("> Saídas:");
      listaSaidas (*(PROTAG.posicao));
    }
    else
      puts (PROTAG.posicao->curta);

    c = getc (stdin);

    if (c == 'x')
      examinar();
    else if (c == 'm')
      mover();
    else if (c == 'h')
      help();
    else if (c == 'e') {
      puts ("\n> Encerrando o JOGO");
      break;
    }
    else 
      puts ("\n> O JOGO não entendeu seu comando.");
  }

  /* teste
  Elemento *El;
  El = buscaTab(universo, StartMain.nome);

  listaSaidas (*El);
  listaConteudo (*El);
  listaStatus (ObjComandante36);
  */

  return 0;
}