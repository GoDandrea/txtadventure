#ifndef MAIN_H
#define MAIN_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "elemento.h"
#include "hash.h"

#define Total_Elementos 50


typedef struct {

  char *nome;
  char *curta;
  char *longa;
  Boolean tipo;
  void *acoes;
  void *animacao;
  Elemento *posicao;
  Lista inventario;
  Lista atributos;

} Protagonista;


Elemento // Lugares
DefautPlace,
StartHall,
StartMain,
StartComputer,
StartDining,
StartKitchen,
StartSecond,
StartOffice,
StartBedroom,
StartCellar,
GameHall,
GameMain;

Elemento // Objetos
DefaultObject,
ObjPapelParede,
ObjLustre,
ObjSofa,
ObjCortinas,
ObjPortaPorao,
ObjComandante36,
ObjMesa,
ObjCadeira,
ObjTomada,
ObjMonitor,
ObjMesaJantar,
ObjCadeiras,
ObjPortaCozinha,
ObjTapete,
ObjEscada,
ObjEstantes,
ObjLivros,
ObjEscrivaninha,
ObjGaveta,
ObjCama,
ObjCriadoMudo,
ObjArmario,
ObjRoupas,
ObjPilhaCoisas,
ObjChavePorao,
ObjChaveGaveta,
ObjFitaJogo,
ObjManual,
ObjCaboEnergia,
ObjCaboMonitor,
ObjLuzJogo,
ObjParedeJogo;

// Protagonista, ou 'aventureiro'
Protagonista PROTAG; 





void intro (void) {

  char *aux;
  aux = 
"\nMAC 216: projeto 'Aventura!'\n"
"\nO JOGO a seguir trata de TEMAS e IDEIAS.\n"
"O JOGO a seguir trata de JOGOS.\n"
"No JOGO você será o JOGADOR e controlará um AVENTUREIRO através desta interface.\n"
"O AVENTUREIRO conta com sua AJUDA para chegar ao fim do JOGO (dentro de sua limitada capacidade " 
"de expectativa como simulacro VIRTUAL inexistente fora do escopo deste PROGRAMA).\n"
"No JOGO você ajudará o AVENTUREIRO a configurar um COMPUTADOR para que ele jogue um JOGO.\n"
"O JOGO não está COMPLETO. Ainda.\n"
"O JOGO não é baseado em FATOS REAIS, mas talvez seja baseado em FATO FICTÍCIOS.\n"
"\nPara mais informações, entre o comando 'h'.\n";

  puts(aux);

}

void loadElemento (Elemento *elem, TabSim Tab) {
  Boolean erro;
  erro = insereTab (Tab, elem->nome, elem);
  if (erro)
    printf("Erro ao inserir %s.\n", elem->nome);
}


void iniciaProtag (void) {

  PROTAG.nome = "aventureiro";
  PROTAG.curta = "aventureiro";
  PROTAG.longa = 
  "O simulacro virtual de um indivíduo. Por ser desprovido de livre arbítrio, você o controla.\n"
  "Almeja o que você almejar, faz o que você comandar, e existe tão somente durante a duração deste programa.\n";

  PROTAG.tipo = Objeto;
  PROTAG.acoes = NULL;
  PROTAG.posicao = &StartHall;
  PROTAG.animacao = NULL;
  PROTAG.inventario = NULL;

}


void iniciaLugares (TabSim Tab) {

/* TEMPLATE

// lugar
#define PLACE

  PLACE.nome = "";
  PLACE.curta = "> \n";
  PLACE.longa = 
  "> \n"
  "> \n"
  "> \n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &Obj);

  PLACE.detalhe = insereLista (PLACE.detalhe, &);

#undef PLACE

*/


// Configuração padrão para lugares
#define PLACE DefautPlace

  PLACE.ativo = 1;
  PLACE.visivel = 1;
  PLACE.conhecido = 0;
  PLACE.tipo = Lugar;
  PLACE.acoes = NULL;
  PLACE.animacao = NULL;
  PLACE.conteudo = NULL;
  PLACE.detalhe = NULL;

#undef PLACE


// Hall de entrada
#define PLACE StartHall

  PLACE = DefautPlace;

  PLACE.nome = "hall de entrada";
  PLACE.curta = "> Hall de entrada. Simples, mas luxuoso.\n";
  PLACE.longa = 
  "> O hall de entrada de sua casa.\n"
  "> O papel de parede decorado e o pequeno lustre de bronze dão um ar aconchegante ao lugar,"
  " A porta adornada te convidar a entrar.\n"
  "> Há uma única porta fechada ao norte, que leva ao resto da casa.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartMain);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Sala de estar
#define PLACE StartMain

  PLACE = DefautPlace;

  PLACE.nome = "sala de estar";
  PLACE.curta = "> Sala de estar. A decoração não deixa dúvidas; aqui é um verdadeiro lar.\n";
  PLACE.longa = 
  "> A sala de estar da casa.\n"
  "> Um enorme sofá encostado à parede te faz considerar um cochilo, "
  "enquanto as cortinas garantem uma agradável meia-luz.\n"
  "> Há uma escadaria ao noroeste que leva ao segundo andar, e embaixo dela, "
  "uma porta que leva ao porão. Ao leste pode-se ver a sala de jantar, e à nordeste, "
  "a porta da sala do computador.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjSofa);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCortinas);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjEscada);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPortaPorao);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartSecond);
  PLACE.detalhe = insereLista (PLACE.detalhe, &StartCellar);
  PLACE.detalhe = insereLista (PLACE.detalhe, &StartComputer);
  PLACE.detalhe = insereLista (PLACE.detalhe, &StartDining);
  PLACE.detalhe = insereLista (PLACE.detalhe, &StartMain);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Sala do computador
#define PLACE StartComputer

  PLACE = DefautPlace;

  PLACE.nome = "sala do computador";
  PLACE.curta = "> Sala do computador. Grande o suficiente pra sua função, apenas.\n";
  PLACE.longa = 
  "> A sala com o computador da casa.\n"
  "> Há uma mesa simples com uma cadeira em frente. Em cima da mesa está o monitor e o velho "
  "computador Comandante 36. Existe também uma tomada na parede leste.\n"
  "> Há uma única porta à oeste";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjMesa);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCadeira);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjTomada);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjMonitor);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjComandante36);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartMain);
  PLACE.detalhe = insereLista (PLACE.detalhe, &GameHall);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Sala de jantar
#define PLACE StartDining

  PLACE = DefautPlace;

  PLACE.nome = "sala de jantar";
  PLACE.curta = "> Sala de jantar. O espaço é dominado por uma longa mesa de jantar de madeira "
  "escura, polida como um espelho.\n";
  PLACE.longa = 
  "> Uma sala feita para receber não só refeições comuns, mas festas e banquetes.\n"
  "> No centro da sala há uma mesa de jantar de mogno. A completa ausência de objetos "
  "sobre ela deixa seu exímio polimento em evidência. Há oito cadeiras em volta da mesa.\n"
  "> À leste pode-se ver a sala de estar, e ao sul, a porta da cozinha.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCortinas);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjMesaJantar);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCadeiras);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPortaCozinha);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartMain);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Segundo andar (corredor)
#define PLACE StartSecond

  PLACE = DefautPlace;

  PLACE.nome = "segundo andar";
  PLACE.curta = "> Corredor do segundo andar. As escadas e duas portas são ligadas por um belo tapete.\n";
  PLACE.longa = 
  "> Um corredor relativamente simples, mas não menos belo que o resto da casa."
  " Um tapete ornado percorre sua extensão.\n"
  "> À oeste está a porta do seu escritório, e ao sul, a porta do seu quarto. \n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjTapete);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartOffice);
  PLACE.detalhe = insereLista (PLACE.detalhe, &StartBedroom);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Escritório/biblioteca
#define PLACE StartOffice

  PLACE = DefautPlace;

  PLACE.nome = "escritório";
  PLACE.curta = "> Escritório. As paredes são cobertas por estantes de livros, e há uma "
  "única escrivaninha no meio do cômodo.\n";
  PLACE.longa = 
  "> Um escritório e biblioteca. A coleção de livros é impressionante, e a escrivaninha tem um ar "
  "extremamente profissional.\n"
  "> Há apenas as estantes com seus livros, a escrivaninha, uma cadeira, e as belas cortinas atrás.\n"
  "> A única saída do cômodo é a porta atrás de você, à leste.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjEscrivaninha);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCadeira);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCortinas);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjEstantes);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLivros);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartSecond);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Quarto
#define PLACE StartBedroom

  PLACE = DefautPlace;

  PLACE.nome = "quarto";
  PLACE.curta = "> Seu quarto. Aconchegante.\n";
  PLACE.longa = "> O melhor (e único) quarto da casa. A cama te chama, mas agora não é hora de dormir.\n"
  "> Além da confortável cama e o fiel criado mudo ao seu lado, o quarto tem também um belo armário. "
  "Cortinas cobrem a maior parte da parede sul.\n"
  "> A única saída do quarto é a porta ao norte.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCortinas);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCama);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCriadoMudo);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjArmario);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartSecond);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Porão
#define PLACE StartCellar

  PLACE = DefautPlace;

  PLACE.nome = "porão";
  PLACE.curta = "> O porão da casa, iluminado apenas por uma estreita janela próxima ao teto.\n";
  PLACE.longa = 
  "> O porão não está tão empoeirado quanto costumava ser.\n"
  "> A principal decoração do cômodo suberrâneo são uma pilha de coisas velhas. \n"
  "> A única saída é a porta ao norte.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPilhaCoisas);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartMain);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Hall de entrada no jogo
#define PLACE GameHall

  PLACE = DefautPlace;

  PLACE.nome = "hall do jogo";
  PLACE.curta = ">> Você está no hall de entrada de uma casa.\n";
  PLACE.longa = 
  ">> Um hall.\n"
  ">> Não há decoração; as paredes são brancas e lisas. Uma única luz branca "
  "ilumina o ambiente.\n"
  ">> Há uma única porta igualmente ascética ao norte.\n";

  PLACE.ativo = 0;
  PLACE.visivel = 0;

  insereLista (PLACE.conteudo, &ObjLuzJogo);
  insereLista (PLACE.conteudo, &ObjParedeJogo);

  insereLista (PLACE.detalhe, &GameMain);
  insereLista (PLACE.detalhe, &StartComputer);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Fim da demo
#define PLACE GameMain

  PLACE = DefautPlace;

  PLACE.nome = "sala";
  PLACE.curta = ">> Você está numa sala.\n";
  PLACE.longa = 
  ">> Você está numa sala.\n";

  loadElemento (&PLACE, Tab);

#undef PLACE

}


void iniciaObjetos (TabSim Tab) {

/* TEMPLATE

// objeto
#define OBJ

  OBJ = DefaultObject;

  OBJ.nome = "";
  OBJ.curta = "> \n";

#undef OBJ
*/


// Configuração padrão para objetos
#define OBJ DefaultObject

  OBJ.longa = OBJ.curta;
  OBJ.ativo = 1;
  OBJ.visivel = 1;
  OBJ.conhecido = 0;
  OBJ.tipo = Objeto;
  OBJ.acoes = NULL;
  OBJ.animacao = NULL;
  OBJ.conteudo = NULL;
  OBJ.detalhe = NULL;

#undef OBJ


// Papel de parede
#define OBJ ObjPapelParede

  OBJ = DefaultObject;

  OBJ.nome = "papel de parede";
  OBJ.curta = "> O papel adornado que reveste as paredes do cômodo.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Lustre
#define OBJ ObjLustre

  OBJ = DefaultObject;

  OBJ.nome = "lustre";
  OBJ.curta = "> Um lustre de bronze polido.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Sofá
#define OBJ ObjSofa

  OBJ = DefaultObject;

  OBJ.nome = "sofa";
  OBJ.curta = "> Um sofá grande e macio. Propício pra uma soneca.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cortinas
#define OBJ ObjCortinas

  OBJ = DefaultObject;

  OBJ.nome = "cortinas";
  OBJ.curta = "> Cortinas grossas e escuras que filtram a luz de fora.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Porta do porão
#define OBJ ObjPortaPorao

  OBJ = DefaultObject;

  OBJ.nome = "porta do porão";
  OBJ.curta = "> Uma porta comum que leva ao porão.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "Trancada");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Commandante 36
#define OBJ ObjComandante36

  OBJ = DefaultObject;

  OBJ.nome = "comandante 36";
  OBJ.curta = "> Um computador modelo Comandante 36.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "\bSem cabo de vídeo");
  OBJ.detalhe = insereLista (OBJ.detalhe, "\bSem cabo de energia");
  OBJ.detalhe = insereLista (OBJ.detalhe, "\bSem fita de programa");
  OBJ.detalhe = insereLista (OBJ.detalhe, "Desligado");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Mesa
#define OBJ ObjMesa

  OBJ = DefaultObject;

  OBJ.nome = "mesa";
  OBJ.curta = "> Uma mesa simples. Tem quatro pernas e um tampo plano.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cadeira
#define OBJ ObjCadeira

  OBJ = DefaultObject;

  OBJ.nome = "cadeira";
  OBJ.curta = "> Uma cadeira simples. Mais confortável que um banco e menos que uma poltrona.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Tomada
#define OBJ ObjTomada

  OBJ = DefaultObject;

  OBJ.nome = "tomada";
  OBJ.curta = "> Uma tomada elétrica. Parece uma cara surpresa.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "Sem plugues");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Monitor
#define OBJ ObjMonitor

  OBJ = DefaultObject;

  OBJ.nome = "monitor";
  OBJ.curta = "> Um monitor velho, capaz apenas de exibir caracteres verdes.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "Desligado");
  OBJ.detalhe = insereLista (OBJ.detalhe, "\bSem sinal");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Mesa de jantar
#define OBJ ObjMesaJantar

  OBJ = DefaultObject;

  OBJ.nome = "mesa de jantar";
  OBJ.curta = "> Uma enorme mesa de jantar de mogno. Você vê seu reflexo no tampo polido.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cadeiras
#define OBJ ObjCadeiras

  OBJ = DefaultObject;

  OBJ.nome = "cadeiras";
  OBJ.curta = "> Cadeiras de mogno com assentos estofados. Claramente feitas pra acompanhar a mesa de jantar.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Porta da cozinha
#define OBJ ObjPortaCozinha

  OBJ = DefaultObject;

  OBJ.nome = "porta da cozinha";
  OBJ.curta = "> Uma porta comum que leva à cozinha.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "Trancada");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Tapete
#define OBJ ObjTapete

  OBJ = DefaultObject;

  OBJ.nome = "tapete";
  OBJ.curta = "> Um longo e elegante tapete. Parece feito sob medida para o corredor.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ 


// Escada
#define OBJ ObjEscada

  OBJ = DefaultObject;

  OBJ.nome = "escada";
  OBJ.curta = "> Uma escada de madeira que liga os dois andares principais da casa.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Estantes de livro
#define OBJ ObjEstantes

  OBJ = DefaultObject;

  OBJ.nome = "estantes";
  OBJ.curta = "> Estantes de madeira abarrotadas de livros.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjLivros);

  loadElemento (&OBJ, Tab);

#undef OBJ


// Livros
#define OBJ ObjLivros

  OBJ = DefaultObject;

  OBJ.nome = "livros";
  OBJ.curta = "> Incontáveis livros sobre uma miríade de tópicos.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjManual);

  loadElemento (&OBJ, Tab);

#undef OBJ


// Escrivaninha
#define OBJ ObjEscrivaninha

  OBJ = DefaultObject;

  OBJ.nome = "escrivaninha";
  OBJ.curta = "> Uma elegante escrivaninha com ar muito profissional e uma discreta gaveta pendurada sob o tampo.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjGaveta);

  loadElemento (&OBJ, Tab);

#undef OBJ

// Gaveta da escrivaninha
#define OBJ ObjGaveta

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "gaveta da escrivaninha";
  OBJ.curta = "> Uma pequena gaveta com fechadura.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjFitaJogo);

  OBJ.detalhe = insereLista (OBJ.detalhe, "Trancada");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cama
#define OBJ ObjCama

  OBJ = DefaultObject;

  OBJ.nome = "cama";
  OBJ.curta = "> Uma cama de casal, grande e fofa. Se você fosse a Cachinhos Dourados, esta seria a cama da Mamãe Ursa.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Criado mudo
#define OBJ ObjCriadoMudo

  OBJ = DefaultObject;

  OBJ.nome = "criado mudo";
  OBJ.curta = "> Um confiável criado mudo com gaveta.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjChavePorao);
  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjChaveGaveta);

  loadElemento (&OBJ, Tab);

#undef OBJ


// Armario
#define OBJ ObjArmario

  OBJ = DefaultObject;

  OBJ.nome = "armario";
  OBJ.curta = "> Um armário, grande e escuro. Talvez guarde roupas, talvez guarde Nárnia.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjRoupas);

  loadElemento (&OBJ, Tab);

#undef OBJ


// Roupas
#define OBJ ObjRoupas

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "roupas";
  OBJ.curta = "> Suas roupas. Definitivamente não são Nárnia.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Pilha de coisas
#define OBJ ObjPilhaCoisas

  OBJ = DefaultObject;

  OBJ.nome = "pilha de coisas";
  OBJ.curta = "> Uma pilha de coisas velhas. Quando que ela passou a ser grande o suficiente pra ser considerado uma pilha?\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjCaboMonitor);
  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjCaboEnergia);

  loadElemento (&OBJ, Tab);

#undef OBJ


// Luz do jogo
#define OBJ ObjLuzJogo

  OBJ = DefaultObject;

  OBJ.nome = "luz";
  OBJ.curta = ">> Fonte de luz.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Paredes do jogo
#define OBJ ObjParedeJogo

  OBJ = DefaultObject;

  OBJ.nome = "paredes";
  OBJ.curta = ">> Paredes.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Chave do porão
#define OBJ ObjChavePorao

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "chave do porão";
  OBJ.curta = "> Chave da porta do porão.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Chave da gaveta
#define OBJ ObjChaveGaveta

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "chave pequena";
  OBJ.curta = "> Uma pequena chave, de um gaveteiro ou algo semelhante.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Fita do jogo
#define OBJ ObjFitaJogo

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "fita cassete";
  OBJ.curta = "> Uma fita cassete com um programa de computador. A etiqueta escrita à mão diz apenas \"JOGO\".\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Manual
#define OBJ ObjManual

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "manual de instruções";
  OBJ.curta = "> O manual de instruções do seu Comandante 36. Vale a pena não perder.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "Para executar o programa de uma fita, apenas ligue o computador com a fita já inserida no slot adequado.");
  OBJ.detalhe = insereLista (OBJ.detalhe, "Para ligar o computador, certifique-se que ele está conectado a um monitor e a uma fonte de energia, e depois aperte o botão.");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cabo de energia
#define OBJ ObjCaboEnergia

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "cabo de energia";
  OBJ.curta = "> Um cabo para ligar um computador à rede elétrica.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cabo de video
#define OBJ ObjCaboMonitor

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "cabo de video";
  OBJ.curta = "> Um cabo para ligar um computador à um monitor ou TV.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ

}



#endif