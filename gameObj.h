#ifndef GAMEOBJ_H
#define GAMEOBJ_H

#include "gameData.h"
#include "elemento.h"
#include "hash.h"



void iniciaObjetos (TabSim Tab) {

/* TEMPLATE

// objeto
#define OBJ

  OBJ = DefaultObject;

  OBJ.nome = "";
  OBJ.curta = "> \n";

#undef OBJ
*/


// Configuração padrão para objetos
#define OBJ DefaultObject

  OBJ.longa = OBJ.curta;
  OBJ.ativo = 1;
  OBJ.visivel = 1;
  OBJ.conhecido = 0;
  OBJ.tipo = Objeto;
  OBJ.acoes = NULL;
  OBJ.animacao = NULL;
  OBJ.conteudo = NULL;
  OBJ.detalhe = NULL;

#undef OBJ


// Papel de parede
#define OBJ ObjPapelParede

  OBJ = DefaultObject;

  OBJ.nome = "papel de parede";
  OBJ.curta = "> O papel adornado que reveste as paredes do cômodo.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Lustre
#define OBJ ObjLustre

  OBJ = DefaultObject;

  OBJ.nome = "lustre";
  OBJ.curta = "> Um lustre de bronze polido.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Sofá
#define OBJ ObjSofa

  OBJ = DefaultObject;

  OBJ.nome = "sofa";
  OBJ.curta = "> Um sofá grande e macio. Propício pra uma soneca.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cortinas
#define OBJ ObjCortinas

  OBJ = DefaultObject;

  OBJ.nome = "cortinas";
  OBJ.curta = "> Cortinas grossas e escuras que filtram a luz de fora.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Porta do porão
#define OBJ ObjPortaPorao

  OBJ = DefaultObject;

  OBJ.nome = "porta do porão";
  OBJ.curta = "> Uma porta comum que leva ao porão.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "Trancada");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Commandante 36
#define OBJ ObjComandante36

  OBJ = DefaultObject;

  OBJ.nome = "comandante 36";
  OBJ.curta = "> Um computador modelo Comandante 36.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "\bSem cabo de vídeo");
  OBJ.detalhe = insereLista (OBJ.detalhe, "\bSem cabo de energia");
  OBJ.detalhe = insereLista (OBJ.detalhe, "\bSem fita de programa");
  OBJ.detalhe = insereLista (OBJ.detalhe, "Desligado");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Mesa
#define OBJ ObjMesa

  OBJ = DefaultObject;

  OBJ.nome = "mesa";
  OBJ.curta = "> Uma mesa simples. Tem quatro pernas e um tampo plano.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cadeira
#define OBJ ObjCadeira

  OBJ = DefaultObject;

  OBJ.nome = "cadeira";
  OBJ.curta = "> Uma cadeira simples. Mais confortável que um banco e menos que uma poltrona.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Tomada
#define OBJ ObjTomada

  OBJ = DefaultObject;

  OBJ.nome = "tomada";
  OBJ.curta = "> Uma tomada elétrica. Parece uma cara surpresa.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "Sem plugues");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Monitor
#define OBJ ObjMonitor

  OBJ = DefaultObject;

  OBJ.nome = "monitor";
  OBJ.curta = "> Um monitor velho, capaz apenas de exibir caracteres verdes.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "Desligado");
  OBJ.detalhe = insereLista (OBJ.detalhe, "\bSem sinal");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Mesa de jantar
#define OBJ ObjMesaJantar

  OBJ = DefaultObject;

  OBJ.nome = "mesa de jantar";
  OBJ.curta = "> Uma enorme mesa de jantar de mogno. Você vê seu reflexo no tampo polido.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cadeiras
#define OBJ ObjCadeiras

  OBJ = DefaultObject;

  OBJ.nome = "cadeiras";
  OBJ.curta = "> Cadeiras de mogno com assentos estofados. Claramente feitas pra acompanhar a mesa de jantar.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Porta da cozinha
#define OBJ ObjPortaCozinha

  OBJ = DefaultObject;

  OBJ.nome = "porta da cozinha";
  OBJ.curta = "> Uma porta comum que leva à cozinha.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "Trancada");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Tapete
#define OBJ ObjTapete

  OBJ = DefaultObject;

  OBJ.nome = "tapete";
  OBJ.curta = "> Um longo e elegante tapete. Parece feito sob medida para o corredor.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ 


// Escada
#define OBJ ObjEscada

  OBJ = DefaultObject;

  OBJ.nome = "escada";
  OBJ.curta = "> Uma escada de madeira que liga os dois andares principais da casa.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Estantes de livro
#define OBJ ObjEstantes

  OBJ = DefaultObject;

  OBJ.nome = "estantes";
  OBJ.curta = "> Estantes de madeira abarrotadas de livros.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjLivros);

  loadElemento (&OBJ, Tab);

#undef OBJ


// Livros
#define OBJ ObjLivros

  OBJ = DefaultObject;

  OBJ.nome = "livros";
  OBJ.curta = "> Incontáveis livros sobre uma miríade de tópicos.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjManual);

  loadElemento (&OBJ, Tab);

#undef OBJ


// Escrivaninha
#define OBJ ObjEscrivaninha

  OBJ = DefaultObject;

  OBJ.nome = "escrivaninha";
  OBJ.curta = "> Uma elegante escrivaninha com ar muito profissional e uma discreta gaveta pendurada sob o tampo.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjGaveta);

  loadElemento (&OBJ, Tab);

#undef OBJ

// Gaveta da escrivaninha
#define OBJ ObjGaveta

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "gaveta da escrivaninha";
  OBJ.curta = "> Uma pequena gaveta com fechadura.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjFitaJogo);

  OBJ.detalhe = insereLista (OBJ.detalhe, "Trancada");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cama
#define OBJ ObjCama

  OBJ = DefaultObject;

  OBJ.nome = "cama";
  OBJ.curta = "> Uma cama de casal, grande e fofa. Se você fosse a Cachinhos Dourados, esta seria a cama da Mamãe Ursa.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Criado mudo
#define OBJ ObjCriadoMudo

  OBJ = DefaultObject;

  OBJ.nome = "criado mudo";
  OBJ.curta = "> Um confiável criado mudo com gaveta.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjChavePorao);
  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjChaveGaveta);

  loadElemento (&OBJ, Tab);

#undef OBJ


// Armario
#define OBJ ObjArmario

  OBJ = DefaultObject;

  OBJ.nome = "armario";
  OBJ.curta = "> Um armário, grande e escuro. Talvez guarde roupas, talvez guarde Nárnia.\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjRoupas);

  loadElemento (&OBJ, Tab);

#undef OBJ


// Roupas
#define OBJ ObjRoupas

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "roupas";
  OBJ.curta = "> Suas roupas. Definitivamente não são Nárnia.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Pilha de coisas
#define OBJ ObjPilhaCoisas

  OBJ = DefaultObject;

  OBJ.nome = "pilha de coisas";
  OBJ.curta = "> Uma pilha de coisas velhas. Quando que ela passou a ser grande o suficiente pra ser considerado uma pilha?\n";

  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjCaboMonitor);
  OBJ.conteudo = insereLista (OBJ.conteudo, &ObjCaboEnergia);

  loadElemento (&OBJ, Tab);

#undef OBJ


// Luz do jogo
#define OBJ ObjLuzJogo

  OBJ = DefaultObject;

  OBJ.nome = "luz";
  OBJ.curta = ">> Fonte de luz.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Paredes do jogo
#define OBJ ObjParedeJogo

  OBJ = DefaultObject;

  OBJ.nome = "paredes";
  OBJ.curta = ">> Paredes.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Chave do porão
#define OBJ ObjChavePorao

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "chave do porão";
  OBJ.curta = "> Chave da porta do porão.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Chave da gaveta
#define OBJ ObjChaveGaveta

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "chave pequena";
  OBJ.curta = "> Uma pequena chave, de um gaveteiro ou algo semelhante.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Fita do jogo
#define OBJ ObjFitaJogo

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "fita cassete";
  OBJ.curta = "> Uma fita cassete com um programa de computador. A etiqueta escrita à mão diz apenas \"JOGO\".\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Manual
#define OBJ ObjManual

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "manual de instruções";
  OBJ.curta = "> O manual de instruções do seu Comandante 36. Vale a pena não perder.\n";

  OBJ.detalhe = insereLista (OBJ.detalhe, "Para executar o programa de uma fita, apenas ligue o computador com a fita já inserida no slot adequado.");
  OBJ.detalhe = insereLista (OBJ.detalhe, "Para ligar o computador, certifique-se que ele está conectado a um monitor e a uma fonte de energia, e depois aperte o botão.");

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cabo de energia
#define OBJ ObjCaboEnergia

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "cabo de energia";
  OBJ.curta = "> Um cabo para ligar um computador à rede elétrica.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ


// Cabo de video
#define OBJ ObjCaboMonitor

  OBJ = DefaultObject;
  OBJ.visivel = 0;

  OBJ.nome = "cabo de video";
  OBJ.curta = "> Um cabo para ligar um computador à um monitor ou TV.\n";

  loadElemento (&OBJ, Tab);

#undef OBJ

}

#endif