#include "elemento.h"

void listaSaidas(Elemento atual) {
  Elo *aux;
  if (atual.tipo == Lugar) {
    aux = atual.detalhe;
    for (int i = 1; aux != NULL; ++i) {
      if (((Elemento *)aux->val)->visivel == 1) {
        printf ("> %s  ", ((Elemento *)aux->val)->nome);
        if (!(i % 3))
          printf("\n");
      }
      aux = aux->next;
    }
    puts("\n");
  }

}

void listaConteudo(Elemento atual) {
  Elo *aux;
    aux = atual.conteudo;
    for (int i = 1; aux != NULL; ++i) {
      if (((Elemento *)aux->val)->visivel == 1) {
        printf ("> %s  ", ((Elemento *)aux->val)->nome);
        if (!(i % 3))
          printf("\n");
      }
      aux = aux->next;
    }
    puts("\n");
}

void listaStatus(Elemento atual) {
  Elo *aux;
  if (atual.tipo == Objeto) {
    aux = atual.detalhe;
    puts("> Status:");
    for (int i = 1; aux != NULL; ++i) {
      if (((char *)aux->val)[0] != '\b') {
        printf ("> %s  ", ((char *)aux->val));
        if (!(i % 3))
          printf("\n");
      }
      aux = aux->next;
    }
    puts("\n");
  }

}
