#ifndef ELEMENTO_H
#define ELEMENTO_H

#include <stdlib.h>
#include <stdio.h>

typedef char Boolean;
struct elemento;
typedef enum Tipo {Objeto, Lugar, Verbo} Tipo;

typedef struct Elo {
  struct Elo *next;
  void *val;
} Elo;

typedef Elo *Lista;

typedef struct {
  Lista *tabela;              // vetor de Listas
  int tam;                    // # da tabela
} TabSim;

typedef struct elemento {
  
  char *nome;
  char *curta;
  char *longa;
  Boolean ativo;
  Boolean visivel;
  Boolean conhecido;
  char tipo;             // 0 = Objeto, 1 = Lugar, 2 = Verbo
  void *acoes;
  void *animacao;
  Lista conteudo;
  Lista detalhe;
   
} Elemento;

typedef enum sentido {
  Norte, Nordeste, Leste, Sudeste, 
  Sul, Sudoeste, Oeste, Noroeste,
  Cima, Baixo
} Sentido;

/*
typedef struct {
  char sentido;
  Elemento *val;
} Cardinal;
*/

void listaSaidas (Elemento atual);
void listaConteudo (Elemento atual);
void listaStatus (Elemento atual);

#endif