#include <string.h>
#include "hash.h"

TabSim criaTab (int tam) {
  TabSim t;
  t.tam = tam;
  t.tabela = calloc (tam, sizeof(Lista));
  return t;
}

static unsigned long hash (char *str) {
  unsigned long hash = 5381;          // número mágico
  int c;

  while ((c = *str++))
    hash = ((hash << 5) + hash) + c;  // hash * 33 + c

  return hash;
}

void destroiTab (TabSim t) {
  while (t.tam) {
    destroiLista (t.tabela[--t.tam]);
  }
}

int insereTab (TabSim t, char *n, Elemento *val) {
  int aux = hash(n) % t.tam;
  t.tabela[aux] = insereLista (t.tabela[aux], val);
  if (t.tabela[aux] == NULL)
    return 1;
  return 0;
}


Elemento* buscaTab (TabSim t, char *n) {
  return (buscaLista (t.tabela[hash(n) % t.tam], n));
}

int retiraTab (TabSim t, char *n) {
  void *aux;
  aux = buscaTab(t, n);
  retiraLista(t.tabela[hash(n) % t.tam], aux);
  free (aux);
  return 1;
}



Lista criaLista () {
  return NULL;
}

void destroiLista (Lista l) {
  Elo *aux1, *aux2 = l;
  while (aux2->next) {
    free (aux2->val);
    aux1 = aux2;  
    aux2 = aux2->next;
    free (aux1);
  }
  free (aux2);
}

Elo* insereLista (Lista l, void *val) {
  Elo *new, *old = l;
  new = calloc (1, sizeof(Elo));
  if (!new)
    return NULL;
  new->next = old;
  new->val = val;
  return new;
}

Elemento* buscaLista (Lista l, char *n) {
  if (l == NULL)
    return NULL;
  Elo *elo = l;
  while (strcmp(((Elemento *)elo->val)->nome, n) != 0) {
    if (!elo->next)
      return NULL;
    elo = elo->next;
  }
  return elo->val;
}

Elemento* retiraLista (Lista l, Elemento *val) {
  Elo *aux, *elo = l;
  if (elo->val == val) {
    aux = elo->next;
    free (elo);
    l = aux;
    return val;
  }

  while (elo->next->val != val)
    elo = elo->next;

  aux = elo->next->next;
  free (elo->next);
  elo->next = aux;
  return val;
}

