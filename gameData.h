#ifndef GAMEDATA_H
#define GAMEDATA_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "elemento.h"
#include "hash.h"

#define Total_Elementos 50




extern Elemento // Lugares
DefautPlace,
StartHall,
StartMain,
StartComputer,
StartDining,
StartKitchen,
StartSecond,
StartOffice,
StartBedroom,
StartCellar,
GameHall,
GameMain;


extern Elemento // Objetos
DefaultObject,
ObjPapelParede,
ObjLustre,
ObjSofa,
ObjCortinas,
ObjPortaPorao,
ObjComandante36,
ObjMesa,
ObjCadeira,
ObjTomada,
ObjMonitor,
ObjMesaJantar,
ObjCadeiras,
ObjPortaCozinha,
ObjTapete,
ObjEscada,
ObjEstantes,
ObjLivros,
ObjEscrivaninha,
ObjGaveta,
ObjCama,
ObjCriadoMudo,
ObjArmario,
ObjRoupas,
ObjPilhaCoisas,
ObjChavePorao,
ObjChaveGaveta,
ObjFitaJogo,
ObjManual,
ObjCaboEnergia,
ObjCaboMonitor,
ObjLuzJogo,
ObjParedeJogo;




typedef struct {

  char *nome;
  char *curta;
  char *longa;
  Boolean tipo;
  void *acoes;
  void *animacao;
  Elemento *posicao;
  Lista inventario;
  Lista atributos;

} Protagonista;

// Protagonista, ou 'aventureiro'
extern Protagonista PROTAG; 


// Ponteiro para função
typedef void (*Fptr)(Elemento *, Elemento *);

void help (void) {

  char *aux;
  aux = 
"\nAJUDA:\n\n"
"O jogo está incompleto atualmente. Faltam:\n"
"> Movimentação para as direções cardinais (norte, noroeste, oeste, etc)\n"
"> Quaisquer ações além de se mover e examinar\n"
"> Um parsing menos rudimentar\n"
"> O resto da história\n"
"entre outros. No momento é impossível completar esta demo, já que não é possível interagir com objetos, e por isso é impossível pegar chaves e peças,"
" destrancar portas e gavetas, e realizar a condição de vitória. Falta também uma checagem da condição de vitória (que virá junto com os verbos).\n"
"\nMODO DE JOGAR:\n"
"Os comandos implementados são 'x', 'm', 'e', e obviamente 'h'. Eles significam 'eXaminar', 'Mover-se', 'Exit', e 'Help'.\n"
"Os comandos 'x' e 'm' dependem de um segundo termo, que é o lugar/objeto sendo examinado, ou o destino do seu movimento.\n"
"Como o parsing não está completamente implementado, o alvo do comando deve estar escrito igual a seu nome, inclusive acentos, e case-sensitive.\n"
"Exemplos de comandos:\n"
"'x aqui'\n'x sofá'\n'm sala de estar'\n"
"\nQuando desejar encerrar o programa, digite 'e'.\n";

  puts(aux);

}


void intro (void) {

  char *aux;
  aux = 
"\nMAC 216: projeto 'Aventura!'\n"
"\nO JOGO a seguir trata de TEMAS e IDEIAS.\n"
"O JOGO a seguir trata de JOGOS.\n"
"No JOGO você será o JOGADOR e controlará um AVENTUREIRO através desta interface.\n"
"O AVENTUREIRO conta com sua AJUDA para chegar ao fim do JOGO (dentro de sua limitada capacidade " 
"de expectativa como simulacro VIRTUAL inexistente fora do escopo deste PROGRAMA).\n"
"No JOGO você ajudará o AVENTUREIRO a configurar um COMPUTADOR para que ele jogue um JOGO.\n"
"O JOGO não está COMPLETO. Ainda.\n"
"O JOGO não é baseado em FATOS REAIS, mas talvez seja baseado em FATO FICTÍCIOS.\n"
"\nPara mais informações, entre o comando 'h'.\n";

  puts(aux);

}

void loadElemento (Elemento *elem, TabSim Tab) {
  Boolean erro;
  erro = insereTab (Tab, elem->nome, elem);
  if (erro)
    printf("Erro ao inserir %s.\n", elem->nome);
}


void iniciaProtag (void) {

  PROTAG.nome = "aventureiro";
  PROTAG.curta = "aventureiro";
  PROTAG.longa = 
  "O simulacro virtual de um indivíduo. Por ser desprovido de livre arbítrio, você o controla.\n"
  "Almeja o que você almejar, faz o que você comandar, e existe tão somente durante a duração deste programa.\n";

  PROTAG.tipo = Objeto;
  PROTAG.acoes = NULL;
  PROTAG.posicao = &StartHall;
  PROTAG.animacao = NULL;
  PROTAG.inventario = NULL;

}




/* Para inicializar as funções */
struct initfunc {
  char *fname;
  Fptr fnct;
};

// Verifica se um objeto está presente e visível 
// Retorna 1 se no local, 2 se no inventário, 0 se não existir
int presente(char *nome) {
  if (buscaLista(inventario, nome)) return 2;
  if (buscaLista(Posic->conteudo, nome)) return 1;
  return 0;
}

/* Macros para testar propriedades comuns */
#define Ativo(x) (x)->ativo
#define Visivel(x) (x)->visivel

/* Transfere um elemento para o inventário */
void Pegar(Elemento *o1, Elemento *o2) {
  if (o1->tipo == Lugar) {
    puts("> Você falha.\n");
    return;
  }

  if (Ativo(o1)) {
    if (Visivel(o1)) {
      int r = presente(o1->nome);
      switch (r) {
        case 2:
          printf ("> %s já está em sua posse.\n", o1->nome);
          return;
        case 1:
          /* retira do local */
          Posic->conteudo = retiraLista (Posic->conteudo, o1->nome);
          /* insere no inventário */
          inventario = insereLista (inventario, o1->nome, OBJ,o1);
          printf("> %s adquirido.\n", o1->nome);
          return;
        default:
          printf("> %s indisponível.\n", o1->nome);
          return;
      }
    }
    else
      printf("> %s não encontrado.\n", o1->nome);
  }
  else
    printf("> \"%s\" inexistente.\n", o1->nome);
}

/* Transfere do inventário para o local atual */
void Largar(Elemento *o1, Elemento *o2) {
  if (o1->tipo == Lugar) {
    puts("> Você falha.\n");
    return;
  }
  if (buscaLista(inventario, o1->nome)) {
    /* retira do inventario */
    inventario = retiraLista (inventario, o1->nome);

    /* insere no local */
    Posic->conteudo = insereLista (Posic->conteudo, o1);
    return;
  }
  else {
    /* Em inglês for fun */
    puts("> Não está em sua posse.\n");
  }
}

/* Descreve um Elemento em detalhes */
void Examinar(Elemento *o1, Elemento *o2) {
  TabSim *ptr;

  /* o default é descrever o local atual */
  if (o1 == NULL || o1 == Posic) {
    puts ("");
    puts (PROTAG.Posic->longa);
    puts ("> Saídas:");
    listaSaidas (*PROTAG.Posic);
    puts ("> Objetos à vista:");
    listaConteudo (*PROTAG.Posic);
    return;
  }
  if (o1->tipo == Objeto)
    if (Ativo(o1) && Visivel(o1))
      puts(o1->longa);
    else {
      puts ("> Alvo de inspeção não encontrado.\n");
      puts("> Objetos encontrados:");
      listaConteudo (*PROTAG.Posic);
    }
  else {
    if (buscaLista (Posic->detalhe, o1->nome)) 
      puts ("> É necessário estar dentro de um lugar para inspecioná-lo.\n")
    else
      puts ("> Alvo de inspeção não encontrado.\n");
  }
}

/* descrição curta de um elemento, está incompleta */
void Olhar(Elemento *o1, Elemento *o2) {
  if (o1) 
    puts(o1->curta);
  else
    puts(Posic->curta);
}



#endif