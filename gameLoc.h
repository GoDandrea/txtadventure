#ifndef GAMELOC_H
#define GAMELOC_H

#include <stdlib.h>

#include "gameData.h"
#include "elemento.h"
#include "hash.h"



void iniciaLugares (TabSim Tab) {

/* TEMPLATE

// lugar
#define PLACE

  PLACE.nome = "";
  PLACE.curta = "> \n";
  PLACE.longa = 
  "> \n"
  "> \n"
  "> \n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &Obj);

  PLACE.detalhe = insereLista (PLACE.detalhe, &);

#undef PLACE

*/


// Configuração padrão para lugares
#define PLACE DefautPlace

  PLACE.ativo = 1;
  PLACE.visivel = 1;
  PLACE.conhecido = 0;
  PLACE.tipo = Lugar;
  PLACE.acoes = NULL;
  PLACE.animacao = NULL;
  PLACE.conteudo = NULL;
  PLACE.detalhe = NULL;

#undef PLACE


// Hall de entrada
#define PLACE StartHall

  PLACE = DefautPlace;

  PLACE.nome = "hall de entrada";
  PLACE.curta = "> Hall de entrada. Simples, mas luxuoso.\n";
  PLACE.longa = 
  "> O hall de entrada de sua casa.\n"
  "> O papel de parede decorado e o pequeno lustre de bronze dão um ar aconchegante ao lugar,"
  " A porta adornada te convidar a entrar.\n"
  "> Há uma única porta fechada ao norte, que leva ao resto da casa.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartMain);

  PLACE.animacao = calloc()



  loadElemento (&PLACE, Tab);

#undef PLACE


// Sala de estar
#define PLACE StartMain

  PLACE = DefautPlace;

  PLACE.nome = "sala de estar";
  PLACE.curta = "> Sala de estar. A decoração não deixa dúvidas; aqui é um verdadeiro lar.\n";
  PLACE.longa = 
  "> A sala de estar da casa.\n"
  "> Um enorme sofá encostado à parede te faz considerar um cochilo, "
  "enquanto as cortinas garantem uma agradável meia-luz.\n"
  "> Há uma escadaria ao noroeste que leva ao segundo andar, e embaixo dela, "
  "uma porta que leva ao porão. Ao leste pode-se ver a sala de jantar, e à nordeste, "
  "a porta da sala do computador.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjSofa);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCortinas);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjEscada);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPortaPorao);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartSecond);
  PLACE.detalhe = insereLista (PLACE.detalhe, &StartCellar);
  PLACE.detalhe = insereLista (PLACE.detalhe, &StartComputer);
  PLACE.detalhe = insereLista (PLACE.detalhe, &StartDining);
  PLACE.detalhe = insereLista (PLACE.detalhe, &StartMain);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Sala do computador
#define PLACE StartComputer

  PLACE = DefautPlace;

  PLACE.nome = "sala do computador";
  PLACE.curta = "> Sala do computador. Grande o suficiente pra sua função, apenas.\n";
  PLACE.longa = 
  "> A sala com o computador da casa.\n"
  "> Há uma mesa simples com uma cadeira em frente. Em cima da mesa está o monitor e o velho "
  "computador Comandante 36. Existe também uma tomada na parede leste.\n"
  "> Há uma única porta à oeste";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjMesa);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCadeira);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjTomada);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjMonitor);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjComandante36);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartMain);
  PLACE.detalhe = insereLista (PLACE.detalhe, &GameHall);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Sala de jantar
#define PLACE StartDining

  PLACE = DefautPlace;

  PLACE.nome = "sala de jantar";
  PLACE.curta = "> Sala de jantar. O espaço é dominado por uma longa mesa de jantar de madeira "
  "escura, polida como um espelho.\n";
  PLACE.longa = 
  "> Uma sala feita para receber não só refeições comuns, mas festas e banquetes.\n"
  "> No centro da sala há uma mesa de jantar de mogno. A completa ausência de objetos "
  "sobre ela deixa seu exímio polimento em evidência. Há oito cadeiras em volta da mesa.\n"
  "> À leste pode-se ver a sala de estar, e ao sul, a porta da cozinha.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCortinas);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjMesaJantar);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCadeiras);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPortaCozinha);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartMain);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Segundo andar (corredor)
#define PLACE StartSecond

  PLACE = DefautPlace;

  PLACE.nome = "segundo andar";
  PLACE.curta = "> Corredor do segundo andar. As escadas e duas portas são ligadas por um belo tapete.\n";
  PLACE.longa = 
  "> Um corredor relativamente simples, mas não menos belo que o resto da casa."
  " Um tapete ornado percorre sua extensão.\n"
  "> À oeste está a porta do seu escritório, e ao sul, a porta do seu quarto. \n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjTapete);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartOffice);
  PLACE.detalhe = insereLista (PLACE.detalhe, &StartBedroom);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Escritório/biblioteca
#define PLACE StartOffice

  PLACE = DefautPlace;

  PLACE.nome = "escritório";
  PLACE.curta = "> Escritório. As paredes são cobertas por estantes de livros, e há uma "
  "única escrivaninha no meio do cômodo.\n";
  PLACE.longa = 
  "> Um escritório e biblioteca. A coleção de livros é impressionante, e a escrivaninha tem um ar "
  "extremamente profissional.\n"
  "> Há apenas as estantes com seus livros, a escrivaninha, uma cadeira, e as belas cortinas atrás.\n"
  "> A única saída do cômodo é a porta atrás de você, à leste.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjEscrivaninha);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCadeira);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCortinas);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjEstantes);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLivros);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartSecond);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Quarto
#define PLACE StartBedroom

  PLACE = DefautPlace;

  PLACE.nome = "quarto";
  PLACE.curta = "> Seu quarto. Aconchegante.\n";
  PLACE.longa = "> O melhor (e único) quarto da casa. A cama te chama, mas agora não é hora de dormir.\n"
  "> Além da confortável cama e o fiel criado mudo ao seu lado, o quarto tem também um belo armário. "
  "Cortinas cobrem a maior parte da parede sul.\n"
  "> A única saída do quarto é a porta ao norte.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPapelParede);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjLustre);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCortinas);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCama);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjCriadoMudo);
  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjArmario);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartSecond);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Porão
#define PLACE StartCellar

  PLACE = DefautPlace;

  PLACE.nome = "porão";
  PLACE.curta = "> O porão da casa, iluminado apenas por uma estreita janela próxima ao teto.\n";
  PLACE.longa = 
  "> O porão não está tão empoeirado quanto costumava ser.\n"
  "> A principal decoração do cômodo suberrâneo são uma pilha de coisas velhas. \n"
  "> A única saída é a porta ao norte.\n";

  PLACE.conteudo = insereLista (PLACE.conteudo, &ObjPilhaCoisas);

  PLACE.detalhe = insereLista (PLACE.detalhe, &StartMain);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Hall de entrada no jogo
#define PLACE GameHall

  PLACE = DefautPlace;

  PLACE.nome = "hall do jogo";
  PLACE.curta = ">> Você está no hall de entrada de uma casa.\n";
  PLACE.longa = 
  ">> Um hall.\n"
  ">> Não há decoração; as paredes são brancas e lisas. Uma única luz branca "
  "ilumina o ambiente.\n"
  ">> Há uma única porta igualmente ascética ao norte.\n";

  PLACE.ativo = 0;
  PLACE.visivel = 0;

  insereLista (PLACE.conteudo, &ObjLuzJogo);
  insereLista (PLACE.conteudo, &ObjParedeJogo);

  insereLista (PLACE.detalhe, &GameMain);
  insereLista (PLACE.detalhe, &StartComputer);

  loadElemento (&PLACE, Tab);

#undef PLACE


// Fim da demo
#define PLACE GameMain

  PLACE = DefautPlace;

  PLACE.nome = "sala";
  PLACE.curta = ">> Você está numa sala.\n";
  PLACE.longa = 
  ">> Você está numa sala.\n";

  loadElemento (&PLACE, Tab);

#undef PLACE

}



#endif